import nltk
from nltk.data  import load
from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer()

import numpy
import tflearn
import tensorflow
import random
import json
import pickle

#Dataset Loading
with open("intents.json") as file:
    data = json.load(file)

try:
    with open("./data.pickle", "rb") as f:
        words, labels, x_train, y_train = pickle.load(f)
except:
    words = []  #list of unique words
    labels = []
    docs_x = [] #list of patterns
    docs_y = [] #list of tags
#data extraction
    for intent in data["intents"]:
        for pattern in intent["patterns"]:
            wrds = nltk.word_tokenize(pattern)
            words.extend(wrds)
            docs_x.append(wrds)
            docs_y.append(intent["tag"])

        if intent["tag"] not in labels:
            labels.append(intent["tag"])

    words = [stemmer.stem(w.lower()) for w in words if w != "?"]
    words = sorted(list(set(words)))

    labels = sorted(labels)

    x_train = []
    y_train = []

    out_empty = [0 for _ in range(len(labels))]

    for x, doc in enumerate(docs_x):
        bag = []

        wrds = [stemmer.stem(w.lower()) for w in doc]
#bag of words
        for w in words:
            if w in wrds:
                bag.append(1)
            else:
                bag.append(0)

        output_row = out_empty[:]
        output_row[labels.index(docs_y[x])] = 1

        x_train.append(bag)
        y_train.append(output_row)


    x_train = numpy.array(x_train)
    y_train = numpy.array(y_train)

    with open("data.pickle", "wb") as f:
        pickle.dump((words, labels, x_train, y_train), f)


#ANN

tensorflow.reset_default_graph()

net = tflearn.input_data(shape=[None, len(x_train[0])]) #input layer
net = tflearn.fully_connected(net, 8)#first hidden layer 
net = tflearn.fully_connected(net, 8)  #second hidden layer
net = tflearn.fully_connected(net, len(y_train[0]), activation="softmax")#output layer with softmax as activation function"
net = tflearn.regression(net)#evaluate the model (optimizer and loss function)


model = tflearn.DNN(net)#model builded

try:
    #loading the model
    model.load("model.tflearn")
except:
   
    #saving the model
    model.fit(x_train, y_train, n_epoch=1000, batch_size=8, show_metric=True)
    model.save("model.tflearn")



def bag_of_words(s, words):
    bag = [0 for _ in range(len(words))]

    s_words = nltk.word_tokenize(s)
    s_words = [stemmer.stem(word.lower()) for word in s_words]

    for se in s_words:
        for i, w in enumerate(words):
            if w == se:
                bag[i] = 1
            
    return numpy.array(bag)


def chat(msg):
    #print("Start talking with the bot (type quit to stop)!")
    #while True:
    print(msg)
    #inp = input("You: ")
    inp = msg
    if inp.lower() == "quit":
        print("really worlking")
#test phase
    results = model.predict([bag_of_words(inp, words)])[0]
    results_index = numpy.argmax(results)
    tag = labels[results_index]

    if results[results_index] > 0.7:
        for tg in data["intents"]:
            if tg['tag'] == tag:
                responses = tg['responses']

        return (random.choice(responses))
    else:
        answer="I don't understand so plz ask another question"
        return (answer)
            
