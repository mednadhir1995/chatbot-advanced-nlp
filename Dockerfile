# Dockerfile -
FROM python:3.6
WORKDIR /chatbot

COPY ./main.py .
ENTRYPOINT ["python", "main.py"]
COPY ./requirements.txt .
COPY ./template/index.html .
COPY ./load_predict.py .
COPY ./Making-Model&Saving-it.py .
COPY ./model.tflearn.data-00000-of-00001 .
COPY  ./model.tflearn.index .
COPY ./model.tflearn.meta .
COPY ./intents.json .
RUN pip install -r requirements.txt
RUN python -c "import nltk; nltk.download('punkt')"
COPY ./main.py .

CMD ["python","main.py"]


#RUN pip install virtualenv
#RUN virtualenv -p python3.7.2 envs
#COPY ./requirements.txt .
#RUN (chatbot docker)/Scripts/activate && pip install -r requirements.txt
#RUN /bin/bash -c "source /virtual/bin/activate" && pip install -r requirements.txt
#RUN /bin/bash -c "./Scripts/activate" && pip install -r requirements.txt
#COPY ./Making Model& Saving it.py .
#COPY ./model.tflearn.* .
#RUN /bin/bash -c "./envs/Scripts/activate.bat" && 
