import nltk
from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer()

import numpy
import tflearn
import tensorflow
import random
import json
import pickle

with open("intents.json") as file:
    data = json.load(file)

try:
    with open("data.pickle", "rb") as f:
        words, labels, training, output = pickle.load(f)
except:
    words = []
    labels = []
    docs_x = []
    docs_y = []

    for intent in data["intents"]:
        for pattern in intent["patterns"]:
            wrds = nltk.word_tokenize(pattern)
            words.extend(wrds)
            docs_x.append(wrds)
            docs_y.append(intent["tag"])

        if intent["tag"] not in labels:
            labels.append(intent["tag"])
          

    words = [stemmer.stem(w.lower()) for w in words if w != "?"]
    words = sorted(list(set(words)))

    labels = sorted(labels)
print(labels)    

    training = []
    output = []


    out_empty = [0 for _ in range(len(labels))]
    print( out_empty)
  
    for x, doc in enumerate(docs_x):
        bag = []

        wrds = [stemmer.stem(w.lower()) for w in doc]

        for w in words:
            if w in wrds:
                bag.append(1)
            else:
                bag.append(0)

        output_row = out_empty[:]
        output_row[labels.index(docs_y[x])] = 1

        training.append(bag)
        output.append(output_row)


    x_train = numpy.array(training)
    y_train = numpy.array(output)

    with open("data.pickle", "wb") as f:
        pickle.dump((words, labels, training, output), f)
print(training)
print(output)        

tensorflow.reset_default_graph()

net = tflearn.input_data(shape=[None, len(x_train[0])]) #input layer
net = tflearn.fully_connected(net, 8)  #first hidden layer 
net = tflearn.fully_connected(net, 8)  #second hidden layer
net = tflearn.fully_connected(net, len(y_train[0]), activation="softmax") #output layer with softmax as activation function"
net = tflearn.regression(net) #evaluate the model (optimizer and loss function)

model = tflearn.DNN(net) #model builded

#saving the model
model.fit(x_train, y_train, n_epoch=1000, batch_size=8, show_metric=True)
model.save("model.tflearn")
"""